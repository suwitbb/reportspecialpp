import { BrowserModule } from '@angular/platform-browser';
import { NgModule, } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import {LoginModule} from './login/login.module';
import { AppComponent } from './app.component';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { AdminModule } from './admin/admin.module';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import {environment} from '../environments/environment';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ClarityModule,
    BrowserAnimationsModule,
    LoginModule,HttpClientModule,AdminModule
  ],
 providers:[
  { provide: LocationStrategy, useClass: HashLocationStrategy },
  { provide: 'API_URL', useValue: environment.apiUrl }
 ],
  bootstrap: [AppComponent]
})
export class AppModule { }
