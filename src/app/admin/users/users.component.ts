import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styles: []
})
export class UsersComponent implements OnInit {
  users: any = [];
  constructor(private usersService: UsersService) { }

  ngOnInit() {
    this.getusers();
  }

  async getusers() {
    try {
      let token = sessionStorage.getItem('token');
      var rs: any = await this.usersService.getUser(token);
      console.log('User ===>', rs);
      if (rs.ok) {
        this.users = rs.rows;
      }
    } catch (error) {

    }


  }
}
