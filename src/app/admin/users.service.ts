import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { async } from '@angular/core/testing';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(@Inject('API_URL') private apiUrl:string,private httpClient:HttpClient) { }
  async getUser(token:string){
    let url=this.apiUrl+'/user/detial?token='+token;
    return await this.httpClient.get(url).toPromise();
  }
}
