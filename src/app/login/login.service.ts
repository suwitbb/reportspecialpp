import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { async } from '@angular/core/testing';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(@Inject('API_URL') private apiUrl:string,private httpClient:HttpClient) { 
  }
  async  doLogin(username:string,password:string){
let url= this.apiUrl+'/login';
let data ={
  username:username,password:password
}
return  await  this.httpClient.post(url,data).toPromise();
  }
}
