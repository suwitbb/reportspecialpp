import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { async } from '@angular/core/testing';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {
username:string
password:string;
isError:boolean;
txt:string;
  constructor( private loginService:LoginService,private router: Router) { }

  ngOnInit() {
  }
  async  doLogin(){
if(this.username && this.password){
try {

  var rs:any= await this.loginService.doLogin(this.username,this.password);
  
  if(rs.ok){
sessionStorage.setItem('token',rs.token);
 this.router.navigateByUrl('/admin');
  }else{
    this.isError=true;
    this.txt=rs.error;
    ;
  }
} catch (error) {

  this.isError=true;
}
}
  }

}
